# Rust Audio Processing

rap aims to be a high quality library to perform signal processing in rust, in particular related to digital speech and audio signals.

Today, it provides the following features:
* Compute the Short Time Fourier Transform (STFT)
* Compute the spectrogram

Please, read the [API documentation here](https://docs.rs/rap)

## License and Contribution

This program is free software.
Any contribution is appreciated: feel free to open Issues and/or Pull/Merge requests. 
They are intended to be under a license compatible with the GNU LGPL.

## Changes

*0.1.0 First version of the library. Supports STFT and spectrograms.
